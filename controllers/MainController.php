<?php
/**
 * Created by PhpStorm.
 * User: andrei
 * Date: 09.06.16
 * Time: 10:46
 */

namespace app\controllers;


use app\models\Item;
use app\models\StoreForm;
use Yii;
use yii\helpers\Url;
use yii\web\Controller;

class MainController extends Controller
{
    public function actionIndex()
    {
        $request = Yii::$app->request;
        $model = new StoreForm();
        $order = false;

        if($request->isPost && $model->load($request->post()) && $model->validate()){
            $order = $model->getOrder();
        }

        return $this->render('main', [
            'model' => $request->isPost ? $model : StoreForm::getDefaultInstance(),
            'order' => $order
        ]);
    }

    public function actionGenerateCatalog()
    {
        Item::generateCatalog();
        Yii::$app->session->setFlash('new-catalog', "Новый католог был сгенерирован.");
        return $this->redirect(Url::to(['main/']));
    }
}