<?php
/**
 * Created by PhpStorm.
 * User: andrei
 * Date: 09.06.16
 * Time: 14:37
 */

namespace app\models;


use Yii;
use yii\mongodb\ActiveRecord;

/**
 * @property string _id
 * @property int price
 * @property int totalSum
 */
class Item extends ActiveRecord
{
    const MIN_PRICE = 1;
    const MAX_PRICE = 10000;
    const CATALOG_SIZE = 10000;

    public static function collectionName()
    {
        return 'item';
    }

    public function attributes()
    {
        return [
            '_id',
            'price',
            'totalSum'
        ];
    }

    /**
     * @return int
     */
    private static function getRandomPrice()
    {
        return rand(self::MIN_PRICE, self::MAX_PRICE);
    }

    /**
     * @return int
     */
    private static function cleanCatalog()
    {
        return static::deleteAll();
    }

    /**
     * Генерируем новый каталог и удаляем существующий
     *
     * @return void
     */
    public static function generateCatalog()
    {
        // создаем элементы со случайной ценой
        $items = [];
        for($i=0; $i < self::CATALOG_SIZE; $i++){
            $items[] = ['price' => static::getRandomPrice()];
        }

        // сортируем элементы по возрастанию
        uasort($items, function($a, $b){
            if($a['price'] == $b['price']){
                return 0;
            }
            return ($a['price'] < $b['price']) ? -1 : 1;
        });

        static::cleanCatalog(); // удаляем старый каталог

        $collection = Yii::$app->mongodb->getCollection('item');

        // сохраняем элементы в БД
        $totalSum = 0;
        $counter = 1;
        foreach($items as $item){
            $totalSum += $item['price'];
            $collection->insert([
                'price' => $item['price'],
                'totalSum' => $totalSum,
                'index' => $counter++
            ]);
        }
    }

//    public static function generateTable($prevAmount = 0)
//    {
//        $singleLoopSize = 1000;
//        $items = Item::getList();
//        $collection = Yii::$app->mongodb->getCollection('table');
//
//        if($prevAmount != 0){
//            $prevRow = $collection->findOne(['amount' => intval($prevAmount)])['values'];
//        } else {
//            $prevRow = [];
//            for($p = Item::MIN_PRICE; $p <= Item::MAX_PRICE; $p++){
//                $prevRow[$p] = false;
//            }
//            $collection->insert(['amount' => 0, 'values' => $prevRow]);
//        }
//
//        $newRow = $prevRow;
//        for($i=$prevAmount+1; $i <= $prevAmount+$singleLoopSize; $i++){
//
//            $item = $items[$i-1];
//            $newRow[$item->price] = true;
//
//            for($j = Item::MIN_PRICE; $j <= count($prevRow)-1; $j++){
//                if($newRow[$j] === true && $prevRow[$j] === true){
//                    $newRow[$j+$item->price] = true;
//                }
//            }
//            $collection->insert(['amount' => $i, 'values' => $newRow]);
//            $prevRow = $newRow;
//        }
//        return $i-1;
//    }
}