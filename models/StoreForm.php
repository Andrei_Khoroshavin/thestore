<?php
/**
 * Created by PhpStorm.
 * User: andrei
 * Date: 09.06.16
 * Time: 11:11
 */

namespace app\models;


use Yii;
use yii\base\Model;

class StoreForm extends Model
{
    const MAX_AMOUNT = 10000;
    const MIN_AMOUNT = 1;
    const MIN_PRICE = 1;

    public $amountFrom;
    public $amountTo;
    public $priceFrom;
    public $priceTo;

    private $topicalList = [];
    private $topicalListLength = 0;
    private $priceRelations = [];

    public function attributeLabels()
    {
        return [
            'amountFrom' => 'Количество единиц от',
            'amountTo' => 'Количество единиц до',
            'priceFrom' => 'Цена от',
            'priceTo' => 'Цена до'
        ];
    }

    public function rules()
    {
        return [
            [['amountFrom', 'amountTo', 'priceFrom', 'priceTo'], 'required'],
            [['amountFrom', 'amountTo', 'priceFrom', 'priceTo'], 'integer'],
            [['amountFrom', 'amountTo'], 'integer', 'min' => self::MIN_AMOUNT, 'max' => self::MAX_AMOUNT],
            [['priceFrom', 'priceTo'], 'integer', 'min' => self::MIN_PRICE],
            ['priceTo', function($attribute){
                if(!$this->hasErrors() && $this->priceFrom > $this->priceTo){
                    $this->addError($attribute, 'Неверные входные данные');
                }
            }],
            ['amountTo', function($attribute){
                if(!$this->hasErrors() && $this->amountFrom > $this->amountTo){
                    $this->addError($attribute, 'Неверные входные данные');
                }
            }]
        ];
    }

    /**
     * Находим индекс минимального элемента,
     * с которого выгоднее начинать поиск
     *
     * @param $maxPrice
     * @return bool|int
     * @throws \yii\mongodb\Exception
     */
    private function getMinItemIndex($maxPrice)
    {
        $result = Item::getCollection()
            ->aggregate([
                '$match' => [
                  'totalSum' => [ '$gte' => intval($maxPrice)]
                ]
            ],
            [
                '$group' => [
                    '_id' => NULL,
                    'index' => [ '$min' => '$index' ]
                ]
            ]
            );

        if(isset($result[0])){
            return $result[0]['index'];
        } else {
            return false; // не существует такого элемента
        }
    }

    /**
     * Выполняем поиск наиболее оптимального заказа
     *
     * @return array|bool
     */
    private function searchOrder()
    {
        /* определяем шаг, с которым будем двигаться по циклу */
        $dif = $this->priceTo - $this->priceFrom;
        if($dif > 50){
            $dif /= 50;
        } else {
            $dif = 1;
        }

        for($price = $this->priceTo; $price >= $this->priceFrom; $price-=$dif){

            $order = $this->calculateOrder($price); // создаем оптимальный заказ для данной цены

            $order['price'] = $price-$order['restPrice']; // считаем текущую цену заказа

            // проверяем заказ, на соответствие входным данным
            if($order['price'] <= $this->priceTo
                && $order['price'] >= $this->priceFrom
                && $order['amount'] >= $this->amountFrom
                && $order['amount'] <= $this->amountTo
            ){
                return $order; // возвращаем заказ, как наиболее оптимальный
            }
        }
        return false;
    }

    /**
     * Строим массив, в котором ключем является цена,
     * а значением является ключ элемента, с которого оптимальнее всего
     * начинать строить заказ
     *
     * Элементы берем из $this->topicalList, поэтому перед исполнение данной функции
     * должна быть вызвана $this->setTopicalList()
     *
     */
    private function buildPriceRelations()
    {
        $relations = [];
        $priceCounter = 1;
        for($i=0;$i<$this->topicalListLength; $i++){
            if($priceCounter > $this->priceTo){
                break;
            }

            while($priceCounter <= $this->priceTo && $this->topicalList[$i]['totalSum'] >= $priceCounter){
                $relations[$priceCounter++] = $i;
            }
        }
        $this->priceRelations = $relations;
    }

    /**
     * Подбираем элементы для заказа
     * ориентируясь на его текущую цену
     *
     * Перед исполнением должна быть единожды вызвана функция $this->buildPriceRelations(),
     * для использования $this->priceRelations
     *
     * @param $price
     * @return array
     */
    private function calculateOrder($price)
    {
        $values = []; // корзина - массив из ключей элементов, входящих в данный заказ
        $count = 0; // число элементов в корзине

        // добавляем элементы в заказ, пока стоимость заказа не достигнет максимальной отметки
        // или пока не будет набрано максимальное количество элементов
        while($price > 0 && $count < $this->amountTo){

            // находим ключ наиболее подходящего элемента и его цену
            $itemKey = $this->priceRelations[$price];
            $itemPrice = $this->topicalList[$itemKey]['price'];

            // проверяем - может ли данный предмет поместиться в заказ
            if($price - $itemPrice < 0){
                break; // мы достигли максимума, выходим из цикла
            }

            // обновляем счетчики
            $values[] = $itemKey;
            $price -= $itemPrice;
            $count++;
        }

        // возвращаем заказ
        return [
            'restPrice' => $price,
            'values' => $values,
            'amount' => $count
        ];
    }

    /**
     * Устанавливаем в объекте список актуальных элементов, среди которых
     * наиболее выгодно проводить поиск оптимального решения
     *
     * Список отсортирован по возрастанию цены элементов
     *
     */
    private function setTopicalList()
    {
        if($minIndex = $this->getMinItemIndex($this->priceTo)){
            // берем все элементы с индексами, которые меньше $minIndex
            $this->topicalList =
                Item::find()
                    ->where([
                        'index' => [
                            '$lte' => intval($minIndex)
                        ]
                    ])
                    ->orderBy(['index' => SORT_ASC])
                    ->asArray()
                    ->all();
            $this->topicalListLength = count($this->topicalList);
        }
    }

    // very long - double dynamic
//    private function searchMax($startKey, $restPrice, $values = [])
//    {
//        // если была достигнута максимальная цена
//        if($restPrice == 0){
//            if(count($values) <= $this->amountTo && count($values) >= $this->amountFrom){
//                return $values; // количество в нужном диапазоне, следовательно последовательность нам подходит
//            } else {
//                return false; // количество не входит в нужный диапазон
//            }
//        }
//
//        // если количество превысило допустимую норму
//        if(count($values) > $this->amountTo){
//            return false;
//        }
//
//        $result = [];
//        for($i = $startKey; $i < $this->topicalListLength; $i++){
//            if($this->topicalList[$i]['totalSum'] < $restPrice){
//                break;
//            }
//            if($this->topicalList[$i]['price'] <= $restPrice){
//                if($localResult = $this->searchMax($i+1, $restPrice - $this->topicalList[$i]['price'], array_merge($values, [$this->topicalList[$i]['index']]))){
//                    if(count($localResult) > count($result)){
//                        $result = $localResult;
//                    }
//                }
//            }
//        }
//
//        return $result;
//    }

    /**
     * Возвращаем все данные о заказе
     *
     * @return array
     */
    public function getOrder()
    {
        // запускаем подготовительные функции
        $this->setTopicalList();
        $this->buildPriceRelations();

        // проверяем, имеет ли смысл выполнять поиск заказа
        // или при заданных условиях невозжно его создать
        if( $this->topicalListLength-1 < $this->amountFrom
            || $this->amountTo < $this->priceRelations[$this->priceFrom] - 1
        ){
            $order = false; // заказ невозможно создать
        } else {
            $order = $this->searchOrder(); // ищем заказ
        }

        return [
            'items' => $this->topicalList, // массив элементов
            'answer' => $order
        ];
    }

    /**
     * Создаем модель с данными по умолчанию
     *
     * @return static
     */
    public static function getDefaultInstance()
    {
        $model = new static();
        $model->amountFrom = 800;
        $model->amountTo = 1000;
        $model->priceFrom = 250000;
        $model->priceTo = 350000;
        return $model;
    }
}