<?php
/**
 * Created by PhpStorm.
 * User: andrei
 * Date: 09.06.16
 * Time: 11:07
 */
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\helpers\Url;

/* @var $model \app\models\StoreForm */
/* @var $order array|boolean */
/* @var $this \yii\web\View */

$this->title = 'TheStore';
?>

<section class="container">
    <div class="row">
        <div class="col-sm-12">
            <h2 style="font-family: 'Comfortaa', cursive;">TheStore</h2>
        </div>
        <div class="col-sm-12">
            <a href="<?=Url::to(['main/generate-catalog'])?>" class="btn btn-default">Сгенерировать новый каталог</a>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <?php if(Yii::$app->session->hasFlash('new-catalog')){?>
                <div style="margin-top: 10px;" class="alert alert-success"><?= Yii::$app->session->getFlash('new-catalog')?></div>
            <?php } ?>
        </div>
    </div>
</section>

<section class="container" style="margin-top: 20px">
    <div class="row">
        <div class="col-sm-6">

            <?php $form = ActiveForm::begin([
                'action' => Url::to(['main/']),
                'method' => 'post',
                'validateOnSubmit' => false,
                'validateOnChange' => false,
                'validateOnType' => false,
                'enableClientValidation' => false,
                'enableAjaxValidation' => false,
            ]) ?>

            <div class="row">
                <?= $form->field($model, 'amountFrom',[
                    'options' => [
                        'class' => 'form-group col-sm-6'
                    ]
                ]) ?>
                <?= $form->field($model, 'amountTo',[
                    'options' => [
                        'class' => 'form-group col-sm-6'
                    ]
                ]) ?>
            </div>

            <div class="row">
                <?= $form->field($model, 'priceFrom',[
                    'options' => [
                        'class' => 'form-group col-sm-6'
                    ]
                ]) ?>
                <?= $form->field($model, 'priceTo',[
                    'options' => [
                        'class' => 'form-group col-sm-6'
                    ]
                ]) ?>
            </div>

            <div class="row">
                <div class="col-sm-12">
                    <?=Html::submitButton('Сгенерировать заказ',[
                        'class' => 'btn btn-primary btn-block'
                    ])?>
                </div>
            </div>
            <?php ActiveForm::end() ?>
        </div>
        <div class="col-sm-6">
            <?php if($order['answer']){?>
                <h4>Цена: <?=$order['answer']['price']?></h4>
                <h4>Количество: <?=$order['answer']['amount']?></h4>
                <div class="order-content" style="height: 550px; overflow-y: scroll">
                    <table class="table table-striped table-bordered">
                        <tr>
                            <th>#</th>
                            <th>Цена</th>
                        </tr>
                        <?php $counter=1; foreach($order['answer']['values'] as $value){ ?>
                            <tr>
                                <td><?=$counter++?></td>
                                <td><?=$order['items'][$value]['price']?></td>
                            </tr>
                        <?php } ?>
                    </table>
                </div>
            <?php } elseif($order) {?>
                <h4 class="text-center">Невозможно сформировать заказ с такими условиями</h4>
            <?php } ?>
        </div>
    </div>
</section>